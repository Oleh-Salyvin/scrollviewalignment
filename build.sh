#!/bin/bash

# Set bash script to exit immediatly if any command fail
set -e

echo "build.sh"
echo "Proccess id:"$$

function_clean_all()
{
    echo "clean..."
    rm -drif "${BUILD_DIR}"
    rm -drif "${DERIVED_DATA_DIR}"
    rm -drif "${BUILD_OUTPUT_DIR}"
}
trap function_clean_all SIGINT

function_clean_after_build()
{
    echo "clean..."
    rm -drif "${BUILD_DIR}"
    rm -drif "${DERIVED_DATA_DIR}"
}


# Constants
PROJECT_DIR=$(pwd)
echo "Project root directory:${PROJECT_DIR}"

SCRIPT_DIR="${PROJECT_DIR}/build-scripts"
echo "Script directory:${SCRIPT_DIR}"

BUILD_DIR="${PROJECT_DIR}/Build"
echo "Build directory:${BUILD_DIR}"

DERIVED_DATA_DIR="DerivedData"

BUILD_OUTPUT_DIR="${PROJECT_DIR}/build-output"
BUILD_OUTPUT_RELEASE_DIR="${BUILD_OUTPUT_DIR}/Release"
BUILD_OUTPUT_ARCHIVE_DIR="${BUILD_OUTPUT_DIR}/Archive"

function_clean_all

mkdir -p "${BUILD_OUTPUT_RELEASE_DIR}"
mkdir -p "${BUILD_OUTPUT_ARCHIVE_DIR}"

## Build
# Make executable
( exec chmod a+x "${SCRIPT_DIR}/build-scrollview-alignment-framework.sh" )
# Build universal
( exec "${SCRIPT_DIR}/build-scrollview-alignment-framework.sh" "$PROJECT_DIR" )

function_clean_after_build

## Archive
# Make executable
( exec chmod a+x "${SCRIPT_DIR}/build-archive-scrollview-alignment-framework.sh" )
# Build archive
( exec "${SCRIPT_DIR}/build-archive-scrollview-alignment-framework.sh" "$PROJECT_DIR" )

function_clean_after_build

echo "build.sh-build-successful"

open "${BUILD_OUTPUT_DIR}"

