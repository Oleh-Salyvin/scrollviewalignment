#!/bin/bash

# Set bash script to exit immediatly if any command fail
set -e

echo "build-archive-scrollview-alignment-framework.sh"
echo "Proccess id:"$$

# Constants
eval PROJECT_DIR=$1
echo "Project root directory:${PROJECT_DIR}"

XCPROJECT_NAME="ScrollViewAlignment"

BUILD_DIR="${PROJECT_DIR}/Build/Products"
echo "Build directory:${BUILD_DIR}"

DERIVED_DATA_DIR="${PROJECT_DIR}/DerivedData"

SCHEME_NAME="ScrollViewAlignment"

CONFIGURATION_NAME="Release"
IPHONE_CONFIGURATION_NAME="Release"

PRODUCT_NAME="ScrollViewAlignment"
CONTENTS_FOLDER_PATH="${PRODUCT_NAME}.framework"

BUILD_OUTPUT_DIR="${PROJECT_DIR}/build-output"
BUILD_OUTPUT_ARCHIVE_DIR="${BUILD_OUTPUT_DIR}/Archive"

echo "build..."
#iphoneos
xcodebuild archive -project "${PROJECT_DIR}/${XCPROJECT_NAME}.xcodeproj" -scheme "${SCHEME_NAME}" -configuration "${IPHONE_CONFIGURATION_NAME}" -arch arm64 -arch armv7 -arch armv7s -sdk iphoneos OBJROOT="${BUILD_DIR}" SYMROOT="${BUILD_DIR}" -quiet ONLY_ACTIVE_ARCH=NO

echo "copy..."
ditto "${BUILD_DIR}/${IPHONE_CONFIGURATION_NAME}-iphoneos/${CONTENTS_FOLDER_PATH}" "${BUILD_DIR}/Archive/${CONTENTS_FOLDER_PATH}"

echo "tar..."
tar -czv -C "${BUILD_DIR}/Archive" -f "${BUILD_OUTPUT_ARCHIVE_DIR}/${CONTENTS_FOLDER_PATH}-Archive.tar.gz" "${CONTENTS_FOLDER_PATH}"

echo "Build output directory:${BUILD_OUTPUT_ARCHIVE_DIR}"
echo "build-archive-scrollview-alignment-framework.sh-successful"
