//
//  SVCoordinateSpace.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 23.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVCoordinateSpace
{
    func origin(of scrollView: UIScrollView) -> CGPoint
    {
        return scrollView.layer.bounds.origin
    }
    
    func visibleRect(attributedAlignment: SVAttributedAlignment, scrollView: UIScrollView, keyboardFrame: CGRect) -> CGRect
    {
        if attributedAlignment.isDescendant
        {
            return scrollView.frame
        }
        
        let contentInset: UIEdgeInsets = self.adjustedContentInset(scrollView: scrollView)
        
        let origin = CGPoint(x: scrollView.contentOffset.x + contentInset.left, y: scrollView.contentOffset.y + contentInset.top)
        
        let scrollLayerBoundsInWindow: CGRect = self.convert(view: scrollView, to: scrollView.window!)
        
        let intersectionRect: CGRect = scrollLayerBoundsInWindow.intersection(keyboardFrame)
        
        var size: CGSize = .zero
        
        if intersectionRect.isNull
        {
            size = CGSize(width: scrollView.frame.width - contentInset.left - contentInset.right, height: scrollView.frame.height - contentInset.top)
        }
        else
        {
            size = CGSize(width: scrollView.frame.width - contentInset.left - contentInset.right, height: scrollView.frame.height - contentInset.top - intersectionRect.height)
        }
        
        return CGRect(origin: origin, size: size)
    }
    
    func innerRect(innerAlignmentRect: SVAttributedAlignment.Path.InnerAlignmentRect, attributedAlignment: SVAttributedAlignment, command: SVCommand) -> CGRect
    {
        switch innerAlignmentRect {
        case .visibleRect:
            return self.visibleRect(attributedAlignment: attributedAlignment, scrollView: command.scrollView, keyboardFrame: command.attributedAlignment.contentInset.frameEnd)
        }
    }
    
    func convert(view: UIView, to scrollView: UIScrollView) -> CGRect
    {
        return scrollView.layer.convert(view.layer.bounds, from: view.layer)
    }
    
    func adjusted(point: inout CGPoint, scrollView: UIScrollView, objectRect: CGRect, visibleRect: CGRect)
    {
        let minX: CGFloat = -(self.adjustedContentInset(scrollView: scrollView).left)
        if (point.x < minX)
        {
            point.x = minX
        }
        
        let maxX: CGFloat = scrollView.contentSize.width - scrollView.bounds.width + (self.adjustedContentInset(scrollView: scrollView).right)
        if (point.x > maxX)
        {
            point.x = maxX
        }
        
        if visibleRect.width >= scrollView.contentSize.width && point.x > minX
        {
            point.x = minX
        }
        
        let minY: CGFloat = -(self.adjustedContentInset(scrollView: scrollView).top)
        if (point.y < minY)
        {
            point.y = minY
        }
        
        var maxY: CGFloat = visibleRect.height
        
        if scrollView.contentSize.height > maxY
        {
            maxY = scrollView.contentSize.height - visibleRect.height - self.adjustedContentInset(scrollView: scrollView).top
        }
        
        if visibleRect.height >= scrollView.contentSize.height && point.y > minY
        {
            point.y = minY
        }
        
        if (maxY < point.y)
        {
            point.y = maxY
        }
    }
    
    func distance(from: CGPoint, to: CGPoint) -> CGFloat
    {
        return sqrt(pow(to.x - (from.x), 2) + pow(to.y - (from.y), 2))
    }
    
    func convert(view: UIView, to window: UIWindow) -> CGRect
    {
        return view.layer.convert(view.layer.bounds, to: window.layer)
    }
    
    func adjustedContentInset(scrollView: UIScrollView) -> UIEdgeInsets
    {
        if #available(iOS 11.0, *)
        {
            return scrollView.adjustedContentInset
        }
        
        return scrollView.contentInset
    }
    
    func searchView(type: AnyClass, fromView: UIView, toView: UIView) -> ContiguousArray<UIView>
    {
        var views: ContiguousArray<UIView> = []
        
        var currentView: UIView = fromView.superview!
        
        while toView !== currentView
        {
            if currentView.isKind(of: type)
            {
                views.append(currentView)
            }
            
            currentView = currentView.superview!
        }
        
        views.append(toView)
        
        views.reverse()
        
        return views
    }
}
