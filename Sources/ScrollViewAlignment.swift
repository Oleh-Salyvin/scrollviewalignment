//
//  ScrollViewAlignment.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 02.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

public protocol ScrollViewAlignmentDelegate: AnyObject
{
    func transition(commandName: SVCommand.Name, attributedAlignment: SVAttributedAlignment, scrollView: UIScrollView)
}

final public class ScrollViewAlignment
{
    public required init() {}
    
    public weak var delegate: ScrollViewAlignmentDelegate?
    
    lazy var model = SVModel(scrollViewAlignment: self)
    
    private let transitionCoordinator = SVTransitionCoordinator()
    
    private lazy var group = SVGroup(scrollViewAlignment: self, transitionCoordinator: self.transitionCoordinator)
    
    public func handle(command: SVCommand)
    {
        self.group.handle(command: command)
    }
}
