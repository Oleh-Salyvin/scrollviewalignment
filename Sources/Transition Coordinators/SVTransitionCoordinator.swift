//
//  SVTransitionCoordinator.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 22.07.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class SVTransitionCoordinator
{
    func run(plan: SVPlan)
    {
        if let _action = plan.action()
        {
            plan.status = .running(_action)
            
            _action.run() { (flag) in
                if flag
                {
                    if plan.isEmpty == false
                    {
                        DispatchQueue.main.async {
                            self.run(plan: plan)
                        }
                    }
                    else
                    {
                        plan.status = .finished
                        
                        DispatchQueue.main.async {
                            plan.agent?.finished(plan: plan)
                        }
                    }
                }
                else
                {
                    plan.status = .interruption(_action)
                    
                    DispatchQueue.main.async {
                        plan.agent?.interrupted(plan: plan, action: _action)
                    }
                }
            }
        }
        else
        {
            DispatchQueue.main.async {
                plan.agent?.finished(plan: plan)
            }
        }
    }
}
