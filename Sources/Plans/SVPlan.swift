//
//  SVPlan.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 27.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class SVPlan
{
    enum Name
    {
        case contentInset
        
        case path
        
        case scrollIndicatorInsets
    }
    
    required init(name: Name, command: SVCommand)
    {
        self.name = name

        self.command = command
    }
    
    let name: Name
    
    let command: SVCommand
    
    weak var agent: SVAgent?
    
    private(set) var actions: ContiguousArray<SVActionProtocol> = []
    
    enum Status
    {
        case idle
        
        case ready
        
        case run, running(SVActionProtocol)
        
        case interruption(SVActionProtocol)
        
        case finished
    }
    
    var status: Status = .idle
    
    var isEmpty: Bool
    {
        return self.actions.isEmpty
    }
    
    func add(action: SVActionProtocol)
    {
        self.actions.append(action)
    }
    
    func add(actions: ContiguousArray<SVActionProtocol>)
    {
        self.actions.append(contentsOf: actions)
    }
    
    func removeAll()
    {
        self.actions.removeAll()
    }
    
    func action() -> SVActionProtocol?
    {
        return self.actions.popLast()
    }
}
