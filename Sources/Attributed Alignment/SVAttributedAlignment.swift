//
//  SVAttributedAlignment.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 12.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

public final class SVAttributedAlignment
{
    public required init(descendant: Bool = false)
    {
        self.isDescendant = descendant
    }
    
    public let isDescendant: Bool
    
    public var animation: SVAnimation = SVAnimation(duration: 0.25)
    
    public var contentInset = SVEdgeInsets()
    
    public var scrollIndicatorInsets = SVEdgeInsets()
    
    public enum Path
    {
        case automatic

        case moveTo(Position)

        public enum Position
        {
            case anchor(CGPoint, InnerAlignmentRect)

            case point(CGPoint, InnerAlignmentRect)

            case toRect(InnerAlignmentRect)
        }

        public enum InnerAlignmentRect
        {
            case visibleRect
        }
    }

    public var path: Path = .automatic

    public var offset: UIOffset = .zero

    public var calmArea: Bool = false

    public var calmAreaSize: CGSize = CGSize(width: 160, height: 110)

    public var alignDescendantScrollViews: Bool = false
}

extension SVAttributedAlignment
{
    public static let AnchorPointLeftAlongTop: CGPoint = CGPoint(x: 0, y: 0)
    public static let AnchorPointTopAlongCenter: CGPoint = CGPoint(x: 0.5, y: 0)
    public static let AnchorPointRightAlongTop: CGPoint = CGPoint(x: 1, y: 0)

    public static let AnchorPointLeftAlongCenter: CGPoint = CGPoint(x: 0, y: 0.5)
    public static let AnchorPointCenter: CGPoint = CGPoint(x: 0.5, y: 0.5)
    public static let AnchorPointRightAlongCenter: CGPoint = CGPoint(x: 1, y: 0.5)

    public static let AnchorPointLeftAlongBottom: CGPoint = CGPoint(x: 0, y: 1)
    public static let AnchorPointCenterAlongBottom: CGPoint = CGPoint(x: 0.5, y: 1)
    public static let AnchorPointRightAlongBottom: CGPoint = CGPoint(x: 1, y: 1)
}
