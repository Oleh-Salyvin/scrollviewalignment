//
//  SVEdgeInsets.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 9/28/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

public final class SVEdgeInsets
{
    public required init() {}
    
    public var edgeInsets: UIEdgeInsets = .zero
    
    public var animation: SVAnimation = SVAnimation(duration: 0.25)
    
    @available (iOS, deprecated: 11)
    public enum Mode
    {
        case always
        
        case never
    }
    
    public var mode: Mode = .always

    public var frameEnd: CGRect = .zero
}
