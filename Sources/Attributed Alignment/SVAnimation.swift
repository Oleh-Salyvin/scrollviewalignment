//
//  SVAnimation.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 25.12.2017.
//  Copyright © 2017 Oleh Salyvin. All rights reserved.
//

import UIKit

public struct SVAnimation
{
    public init(delay: CFTimeInterval = 0, duration: CFTimeInterval = 0, dampingRatio: CGFloat = 1, velocity: CGFloat = 0, options: UIView.AnimationOptions? = nil)
    {
        self.delay = delay
        
        self.duration = duration
        
        self.dampingRatio = dampingRatio
        
        self.velocity = velocity
        
        if options != nil
        {
            self.options = options!
        }
    }
    
    
    public var delay: CFTimeInterval
    
    public var duration: CFTimeInterval
    
    public var dampingRatio: CGFloat
    
    public var velocity: CGFloat
    
    public var options: UIView.AnimationOptions = [UIView.AnimationOptions.allowUserInteraction, UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews, UIView.AnimationOptions.curveLinear]
}
