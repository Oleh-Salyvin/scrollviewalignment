//
//  Value Types.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 21.05.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

struct SVScrollViewSnapshot
{
    unowned let scrollView: UIScrollView
    
    let contentInset: UIEdgeInsets
    
    let scrollIndicatorInsets: UIEdgeInsets
}
