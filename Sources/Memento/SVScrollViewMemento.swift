//
//  SVScrollViewMemento.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 15.05.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVScrollViewMemento
{
    var isEmpty: Bool {
        return self.stack.isEmpty
    }
    
    private var stack: ContiguousArray<SVScrollViewSnapshot> = []
    
    func push(snapshot: SVScrollViewSnapshot)
    {
        self.stack.append(snapshot)
    }
    
    func popLast() -> SVScrollViewSnapshot?
    {
        return self.stack.popLast()
    }
}
