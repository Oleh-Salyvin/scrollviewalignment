//
//  SVContext.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 10/15/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVContext: ScrollViewAlignmentDelegate
{
    //MARK: - ScrollViewAlignmentDelegate
    
    func transition(commandName: SVCommand.Name, attributedAlignment: SVAttributedAlignment, scrollView: UIScrollView)
    {
        if attributedAlignment.isDescendant
        {
            var animation = attributedAlignment.animation
            
            animation.delay = 0.3
            animation.duration = 0.7
            animation.options = [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews, UIView.AnimationOptions.curveLinear]
            
            attributedAlignment.animation = animation
            attributedAlignment.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointLeftAlongCenter, .visibleRect))
        }
        else
        {
            var animation = attributedAlignment.animation
            
            animation.delay = 0.7
            animation.duration = 1
            animation.options = [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews, UIView.AnimationOptions.curveLinear]
            
            attributedAlignment.animation = animation
        }
    }
}
