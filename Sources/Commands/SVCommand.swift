//
//  SVCommand.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 25.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

public class SVCommand
{
    public required init(name: Name, attributedAlignment: SVAttributedAlignment, scrollView: UIScrollView, view: UIView)
    {
        self.name = name

        self.attributedAlignment = attributedAlignment
        
        self.scrollView = scrollView
        
        self.view = view
    }
    
    public enum Name
    {
        case willShow
        
        case willHide
    }
    
    public let name: Name
    
    public let view: UIView
    
    public let scrollView: UIScrollView

    public let attributedAlignment: SVAttributedAlignment
}
