//
//  SVPlanner.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 26.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class SVPlanner
{
    private let contentInsetSolver = SVContentInsetSolver()
    
    private let pathSolver = SVPathSolver()
    
    private let scrollIndicatorInsetsSolver = SVScrollIndicatorInsetsSolver()
    
    private var idlePlans: ContiguousArray<SVPlan> = []
    
    func handle(agent: SVAgent, command: SVCommand)
    {
        self.idlePlans.removeAll()
        
        if let _plan = self.contentInsetSolver.handle(command: command), _plan.isEmpty == false
        {
            print("ContentInset plan found")
            
            self.idlePlans.append(_plan)
        }
        else
        {
            print("ContentInset plan absent. \(command.name)")
        }
        
        if let _plan = self.pathSolver.handle(command: command), _plan.isEmpty == false
        {
            print("Path plan found")
            
            self.idlePlans.append(_plan)
        }
        else
        {
            print("Path plan absent. \(command.name)")
        }
        
        if let _plan = self.scrollIndicatorInsetsSolver.handle(attributedAlignment: command.attributedAlignment, command: command), _plan.isEmpty == false
        {
            print("ScrollIndicatorInsets plan found")
            
            self.idlePlans.append(_plan)
        }
        else
        {
            print("ScrollIndicatorInsets plan absent. \(command.name)")
        }
        
        self.idlePlans.forEach { (_plan) in
            agent.add(plan: _plan)
        }
    }
}
