//
//  SVGroup.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 21.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVGroup
{
    required init(scrollViewAlignment: ScrollViewAlignment, transitionCoordinator: SVTransitionCoordinator)
    {
        self.scrollViewAlignment = scrollViewAlignment
        
        self.transitionCoordinator = transitionCoordinator
        
        self.headAgent = SVAgent(transitionCoordinator: transitionCoordinator)
    }
    
    unowned let scrollViewAlignment: ScrollViewAlignment
    
    unowned let transitionCoordinator: SVTransitionCoordinator
    
    private let headAgent: SVAgent
    
    private var agents: ContiguousArray<SVAgent> = []

    private let coordinateSpace = SVCoordinateSpace()
    
    func handle(command: SVCommand)
    {
        self.updateGroup(command: command)
        
        for _index in self.agents.indices
        {
            self.agents[_index].findPlans()
        }
        
        for _index in self.agents.indices
        {
            self.agents[_index].run()
        }
    }
    
    private func updateGroup(command: SVCommand)
    {
        self.agents.removeAll()
        
        for scrollView in self.coordinateSpace.searchView(type: UIScrollView.self, fromView: command.view, toView: command.scrollView)
        {
            if scrollView === command.scrollView
            {
                self.scrollViewAlignment.model.context().transition(commandName: command.name, attributedAlignment: command.attributedAlignment, scrollView: command.scrollView)
                
                self.headAgent.setup(command: command)
                
                self.agents.append(self.headAgent)
            }
            else if command.attributedAlignment.alignDescendantScrollViews
            {
                let agentDescendant = SVAgent(transitionCoordinator: self.transitionCoordinator)

                let attributedAlignmentDescendant = SVAttributedAlignment(descendant: true)

                let commandDescendant = SVCommand(name: command.name, attributedAlignment: attributedAlignmentDescendant, scrollView: scrollView as! UIScrollView, view: command.view)

                self.scrollViewAlignment.model.context().transition(commandName: commandDescendant.name, attributedAlignment: commandDescendant.attributedAlignment, scrollView: commandDescendant.scrollView)
                
                agentDescendant.setup(command: commandDescendant)
                
                self.agents.append(agentDescendant)
            }
        }
    }
}
