//
//  SVAgent.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 07.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class SVAgent
{
    required init(transitionCoordinator: SVTransitionCoordinator)
    {
        self.transitionCoordinator = transitionCoordinator
    }
    
    unowned let transitionCoordinator: SVTransitionCoordinator
    
    private(set) var command: SVCommand?

    private let pathPlanner = SVPlanner()
    
    private var plans: ContiguousArray<SVPlan> = []
    
    func setup(command: SVCommand)
    {
        self.command = command
    }
    
    func findPlans()
    {
        self.pathPlanner.handle(agent: self, command: self.command!)
    }
    
    func run()
    {
        for _index in self.plans.indices
        {
            let plan = self.plans[_index]
            
            switch plan.status {
            case .ready:
                plan.status = .run
                
                self.transitionCoordinator.run(plan: plan)
                
            default:
                continue
            }
        }
    }
    
    func add(plan: SVPlan)
    {
        plan.agent = self
        
        plan.status = .ready
        
        self.plans.append(plan)
    }
    
    func remove(plan: SVPlan)
    {
        self.plans = self.plans.filter { (_plan) -> Bool in
            debugPrint("\(#function)")
            return plan !== _plan
        }
    }
    
    func finished(plan: SVPlan)
    {
        self.plans = self.plans.filter { (_plan) -> Bool in
            switch _plan.status {
            case .finished:
                debugPrint("\(#function), \(_plan.command.name)")
                return false
            default:
                return true
            }
        }
    }
    
    func interrupted(plan: SVPlan, action: SVActionProtocol)
    {
        self.remove(plan: plan)
    }
}
