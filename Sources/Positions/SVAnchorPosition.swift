//
//  SVAnchorPosition.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 25.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVAnchorPosition
{
    required init() {}

    private let coordinateSpace = SVCoordinateSpace()

    func handle(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> CGPoint?
    {
        guard command.scrollView.window != nil else { return nil }

        var innerRect: CGRect?

        var anchor: CGPoint?

        switch attributedAlignment.path {
        case let .moveTo(_position):
            switch _position {
            case let .anchor(_anchor, _rect):
                anchor = _anchor

                innerRect = self.coordinateSpace.innerRect(innerAlignmentRect: _rect, attributedAlignment: attributedAlignment, command: command)
            default: break
            }
        default: break
        }

        if innerRect != nil
        {
            let objectRect: CGRect = self.coordinateSpace.convert(view: command.view, to: command.scrollView)

            var point = self.point(anchor: anchor!, boundsRect: innerRect!, objectRect: objectRect, attributedAlignment: attributedAlignment, command: command)

            self.coordinateSpace.adjusted(point: &point, scrollView: command.scrollView, objectRect: objectRect, visibleRect: innerRect!)

            if attributedAlignment.calmArea, self.calmArea(point: point, objectRect: objectRect, attributedAlignment: attributedAlignment, command: command)
            {
                return nil
            }

            return point
        }

        return nil
    }

    private func point(anchor: CGPoint, boundsRect: CGRect, objectRect: CGRect, attributedAlignment: SVAttributedAlignment, command: SVCommand) -> CGPoint
    {
        let maxWidth: CGFloat = boundsRect.width - objectRect.width
        let xAlongWidth: CGFloat = maxWidth * anchor.x
        let offsetAlongX: CGFloat = xAlongWidth + (objectRect.width / 2) + attributedAlignment.offset.horizontal

        let x = objectRect.midX - offsetAlongX

        let maxHeight: CGFloat = boundsRect.height - objectRect.height
        let yAlongHeight: CGFloat = maxHeight * anchor.y
        let offsetAlongY: CGFloat = yAlongHeight + (objectRect.height / 2) + attributedAlignment.offset.vertical

        let y = objectRect.midY - offsetAlongY

        return CGPoint(x: x, y: y)
    }

    private func calmArea(point: CGPoint, objectRect: CGRect, attributedAlignment: SVAttributedAlignment, command: SVCommand) -> Bool
    {
        var calmRect: CGRect = .zero

        calmRect.size = attributedAlignment.calmAreaSize

        calmRect.origin = CGPoint(x: self.coordinateSpace.origin(of: command.scrollView).x + point.x - (calmRect.width / 2), y: self.coordinateSpace.origin(of: command.scrollView).y + point.y - (calmRect.height / 2))

        return calmRect.contains(objectRect)
    }
}
