//
//  SVToInnerAlignmentRectPosition.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 25.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVToInnerAlignmentRectPosition
{
    required init() {}

    private let coordinateSpace = SVCoordinateSpace()

    func handle(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> CGPoint?
    {
        guard command.scrollView.window != nil else { return nil }

        var innerRect: CGRect?

        switch attributedAlignment.path {
        case let .moveTo(_position):
            switch _position {
            case let .toRect(_rect):
                innerRect = self.coordinateSpace.innerRect(innerAlignmentRect: _rect, attributedAlignment: attributedAlignment, command: command)
            default: break
            }
        default: break
        }

        let objectRect: CGRect = self.coordinateSpace.convert(view: command.view, to: command.scrollView)

        if innerRect != nil, innerRect!.contains(objectRect) == false
        {
            let objectRectCenter: CGPoint = CGPoint(x: objectRect.origin.x + (objectRect.width / 2), y: objectRect.origin.y + (objectRect.height / 2))

            let p0 = self.coordinateSpace.distance(from: innerRect!.origin, to: objectRectCenter)

            let p0_mid_p1 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.midX, y: innerRect!.minY), to: objectRectCenter)

            let p1 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.maxX, y: innerRect!.minY), to: objectRectCenter)

            let p1_mid_p2 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.maxX, y: innerRect!.midY), to: objectRectCenter)

            let p2 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.maxX, y: innerRect!.maxY), to: objectRectCenter)

            let p2_mid_p3 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.midX, y: innerRect!.maxY), to: objectRectCenter)

            let p3 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.minX, y: innerRect!.maxY), to: objectRectCenter)

            let p3_mid_p0 = self.coordinateSpace.distance(from: CGPoint(x: innerRect!.minX, y: innerRect!.midY), to: objectRectCenter)

            struct SVDistanceRow
            {
                let distance: CGFloat

                let anchor: CGPoint
            }

            let distanceTable: Array<SVDistanceRow> = [
                SVDistanceRow(distance: p0, anchor: CGPoint(x: 0, y: 0)),
                SVDistanceRow(distance: p0_mid_p1, anchor: CGPoint(x: 0.5, y: 0)),
                SVDistanceRow(distance: p1, anchor: CGPoint(x: 1, y: 0)),
                SVDistanceRow(distance: p1_mid_p2, anchor: CGPoint(x: 1, y: 0.5)),
                SVDistanceRow(distance: p2, anchor: CGPoint(x: 1, y: 1)),
                SVDistanceRow(distance: p2_mid_p3, anchor: CGPoint(x: 0.5, y: 1)),
                SVDistanceRow(distance: p3, anchor: CGPoint(x: 0, y: 1)),
                SVDistanceRow(distance: p3_mid_p0, anchor: CGPoint(x: 0, y: 0.5))]

            let selectedDistance = distanceTable.min { (a, b) -> Bool in
                return a.distance.isLess(than: b.distance)
            }

            if selectedDistance != nil
            {
                let attributedAlignmentSelected = SVAttributedAlignment(descendant: attributedAlignment.isDescendant)

                attributedAlignmentSelected.path = .moveTo(.anchor(selectedDistance!.anchor, .visibleRect))

                let position = SVAnchorPosition()

                return position.handle(attributedAlignment: attributedAlignmentSelected, command: command)
            }
        }

        return nil
    }
}
