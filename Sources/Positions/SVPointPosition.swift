//
//  SVPointPosition.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 25.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVPointPosition
{
    required init() {}

    private let coordinateSpace = SVCoordinateSpace()

    func handle(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> CGPoint?
    {
        guard command.scrollView.window != nil else { return nil }

        var innerRect: CGRect?
        
        var point: CGPoint?
        
        switch attributedAlignment.path {
        case let .moveTo(_position):
            switch _position {
            case let .point(_point, _rect):
                point = _point
                
                innerRect = self.coordinateSpace.innerRect(innerAlignmentRect: _rect, attributedAlignment: attributedAlignment, command: command)
            default: break
            }
        default: break
        }
        
        if innerRect != nil
        {
            let objectRect: CGRect = self.coordinateSpace.convert(view: command.view, to: command.scrollView)
            
            var point = self.point(point: point!, innerRect: innerRect!, objectRect: objectRect, attributedAlignment: attributedAlignment)
            
            self.coordinateSpace.adjusted(point: &point, scrollView: command.scrollView, objectRect: objectRect, visibleRect: innerRect!)
            
            if attributedAlignment.calmArea, self.calmArea(point: point, objectRect: objectRect, attributedAlignment: attributedAlignment, command: command)
            {
                return nil
            }
            
            return point
        }
        
        return nil
    }
    
    private func point(point: CGPoint, innerRect: CGRect, objectRect: CGRect, attributedAlignment: SVAttributedAlignment) -> CGPoint
    {
        let offsetAlongX: CGFloat = point.x + attributedAlignment.offset.horizontal
        let x = objectRect.origin.x - offsetAlongX
        
        let offsetAlongY: CGFloat = point.y + attributedAlignment.offset.vertical
        let y = objectRect.origin.y - offsetAlongY
        
        return CGPoint(x: x, y: y)
    }
    
    private func calmArea(point: CGPoint, objectRect: CGRect, attributedAlignment: SVAttributedAlignment, command: SVCommand) -> Bool
    {
        var calmRect: CGRect = .zero
        
        calmRect.size = attributedAlignment.calmAreaSize
        
        calmRect.origin = CGPoint(x: self.coordinateSpace.origin(of: command.scrollView).x + point.x, y: self.coordinateSpace.origin(of: command.scrollView).y + point.y)
        
        return calmRect.contains(objectRect)
    }
}
