//
//  SVModel.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 10/28/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class SVModel
{
    required init(scrollViewAlignment: ScrollViewAlignment)
    {
        self.scrollViewAlignment = scrollViewAlignment
    }
    
    unowned let scrollViewAlignment: ScrollViewAlignment
    
    private var contextDefault: SVContext?
    
    func context() -> ScrollViewAlignmentDelegate
    {
        if self.scrollViewAlignment.delegate != nil
        {
            self.contextDefault = nil
            
            return self.scrollViewAlignment.delegate!
        }
        else if self.contextDefault == nil
        {
            self.contextDefault = SVContext()
        }
        
        return self.contextDefault!
    }
}
