//
//  SVActionProtocol.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 10/15/18.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

protocol SVActionProtocol
{
    func run(completion: ((Bool) -> Void)?)
    
    func halt()
}
