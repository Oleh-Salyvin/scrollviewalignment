//
//  SVPositionAnimation.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 24.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVPositionAnimation: SVActionProtocol
{
    required init(scrollView: UIScrollView, toValue: CGPoint)
    {
        self.scrollView = scrollView
        
        self.toValue = toValue
    }
    
    weak var scrollView: UIScrollView?
    
    var toValue: CGPoint
    
    var animation: SVAnimation?
    
    let keyPath: String = "bounds.origin"
    
    func run(completion: ((Bool) -> Void)? = nil)
    {
        guard self.animation != nil else { return }
        
        UIView.animate(withDuration: self.animation!.duration, delay: self.animation!.delay, usingSpringWithDamping: self.animation!.dampingRatio, initialSpringVelocity: self.animation!.velocity, options: self.animation!.options, animations: {
            
            self.scrollView?.setContentOffset(self.toValue, animated: false)
            
        }, completion: completion)
    }
    
    func halt()
    {
        guard let scrollView = self.scrollView else { return }
        
        let animations = scrollView.layer.animationKeys()?.filter({ (key) -> Bool in
            return key.hasPrefix(self.keyPath)
        })
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        scrollView.layer.bounds.origin = scrollView.layer.presentation()!.bounds.origin
        
        animations?.forEach({ (key) in
            scrollView.layer.removeAnimation(forKey: key)
        })
        
        CATransaction.commit()
    }
}
