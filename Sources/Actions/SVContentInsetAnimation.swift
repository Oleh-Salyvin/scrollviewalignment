//
//  SVContentInsetAnimation.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 29.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVContentInsetAnimation: SVActionProtocol
{
    required init(scrollView: UIScrollView, toValue: UIEdgeInsets)
    {
        self.scrollView = scrollView
        
        self.toValue = toValue
    }
    
    weak var scrollView: UIScrollView?
    
    var toValue: UIEdgeInsets
    
    var animation: SVAnimation?
    
    func run(completion: ((Bool) -> Void)? = nil)
    {
        guard self.animation != nil else { return }
        
        var alignOnlyBottom = true
        
        if #available(iOS 11.0, *)
        {
            if self.scrollView!.contentInsetAdjustmentBehavior == .never
            {
                alignOnlyBottom = false
            }
        }
        
        UIView.animate(withDuration: self.animation!.duration, delay: self.animation!.delay, options: self.animation!.options, animations: {
            if alignOnlyBottom
            {
                self.scrollView?.contentInset.bottom = self.toValue.bottom
            }
            else
            {
                self.scrollView?.contentInset = self.toValue
            }
        }, completion: completion)
    }
    
    func halt() {}
}
