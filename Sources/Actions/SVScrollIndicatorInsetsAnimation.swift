//
//  SVScrollIndicatorInsetsAnimation.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 03.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVScrollIndicatorInsetsAnimation: SVActionProtocol
{
    required init(scrollView: UIScrollView, toValue: UIEdgeInsets)
    {
        self.scrollView = scrollView
        
        self.toValue = toValue
    }
    
    weak var scrollView: UIScrollView?
    
    var toValue: UIEdgeInsets
    
    var animation: SVAnimation?
    
    func run(completion: ((Bool) -> Void)? = nil)
    {
        guard self.animation != nil else { return }
        
        UIView.animate(withDuration: self.animation!.duration, delay: self.animation!.delay, options: self.animation!.options, animations: {
            
            self.scrollView?.scrollIndicatorInsets = self.toValue
            
        }, completion: completion)
    }
    
    func halt() {}
}
