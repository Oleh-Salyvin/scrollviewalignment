//
//  SVContentInsetSolver.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 30.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVContentInsetSolver
{
    required init() {}

    private let coordinateSpace = SVCoordinateSpace()

    private var firstEventReceived: Bool = false
    
    private var memento: SVScrollViewMemento = SVScrollViewMemento()
    
    func handle(command: SVCommand) -> SVPlan?
    {
        guard command.scrollView.window != nil else { return nil }
        
        if command.name == .willShow
        {
            if self.firstEventReceived == false
            {
                self.firstEventReceived = true
                
                self.memento.push(snapshot: SVScrollViewSnapshot(scrollView: command.scrollView, contentInset: self.coordinateSpace.adjustedContentInset(scrollView: command.scrollView), scrollIndicatorInsets: command.scrollView.scrollIndicatorInsets))
            }
            
            return self.update(command: command)
        }
        else if command.name == .willHide
        {
            if self.firstEventReceived
            {
                self.firstEventReceived = false
                
                if let snapshot = self.memento.popLast()
                {
                    if #available(iOS 11.0, *), command.scrollView.contentInsetAdjustmentBehavior == .never
                    {}
                    else if command.attributedAlignment.contentInset.mode == .never
                    {}
                    else
                    {
                        return self.restore(edgeInsets: snapshot.contentInset, command: command)
                    }
                    
                    return nil
                }
            }
        }
        
        return nil
    }
    
    private func update(command: SVCommand) -> SVPlan?
    {
        if command.attributedAlignment.isDescendant
        {
            return updateDescendant(command: command)
        }
        
        return updateRoot(command: command)
    }
    
    private func updateDescendant(command: SVCommand) -> SVPlan?
    {
        return nil
    }
    
    private func updateRoot(command: SVCommand) -> SVPlan?
    {
        var contentInset: UIEdgeInsets = self.coordinateSpace.adjustedContentInset(scrollView: command.scrollView)
        
        let scrollLayerInWindow: CGRect = self.coordinateSpace.convert(view: command.scrollView, to: command.scrollView.window!)
        
        let intersectionRect: CGRect = scrollLayerInWindow.intersection(command.attributedAlignment.contentInset.frameEnd)
        
        if #available(iOS 11.0, *)
        {
            switch command.scrollView.contentInsetAdjustmentBehavior {
            case .automatic:
                contentInset.bottom = command.attributedAlignment.contentInset.frameEnd.size.height - command.scrollView.safeAreaInsets.bottom
                
            case .always, .scrollableAxes:
                if intersectionRect.isNull == false
                {
                    contentInset.bottom = intersectionRect.height
                }
                
            case .never:
                break
            }
        }
        else
        {
            if command.attributedAlignment.contentInset.mode == .always
            {
                if intersectionRect.isNull == false
                {
                    contentInset.bottom = intersectionRect.height
                }
            }
        }
        
        let action = SVContentInsetAnimation(scrollView: command.scrollView, toValue: contentInset)
        
        action.animation = command.attributedAlignment.contentInset.animation
        
        let plan = SVPlan(name: .contentInset, command: command)
        
        plan.add(action: action)
        
        return plan
    }
    
    private func restore(edgeInsets: UIEdgeInsets, command: SVCommand) -> SVPlan?
    {
        let action = SVContentInsetAnimation(scrollView: command.scrollView, toValue: edgeInsets)
        
        action.animation = command.attributedAlignment.contentInset.animation
        
        let plan = SVPlan(name: .contentInset, command: command)
        
        plan.add(action: action)
        
        return plan
    }
}
