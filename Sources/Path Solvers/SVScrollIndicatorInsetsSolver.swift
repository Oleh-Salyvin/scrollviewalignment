//
//  SVScrollIndicatorInsetsSolver.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 20.03.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVScrollIndicatorInsetsSolver
{
    required init() {}

    private let coordinateSpace = SVCoordinateSpace()

    private var firstEventReceived: Bool = false
    
    private let memento: SVScrollViewMemento = SVScrollViewMemento()
    
    func handle(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        guard command.scrollView.window != nil else { return nil }
        
        if command.name == .willShow
        {
            if self.firstEventReceived == false
            {
                self.firstEventReceived = true
                
                self.memento.push(snapshot: SVScrollViewSnapshot(scrollView: command.scrollView, contentInset: command.scrollView.contentInset, scrollIndicatorInsets: command.scrollView.scrollIndicatorInsets))
            }
            
            return self.update(attributedAlignment: attributedAlignment, command: command)
        }
        else if command.name == .willHide
        {
            if self.firstEventReceived
            {
                self.firstEventReceived = false
                
                if let snapshot = self.memento.popLast()
                {
                    if #available(iOS 11.0, *), command.scrollView.contentInsetAdjustmentBehavior == .never
                    {}
                    else if attributedAlignment.contentInset.mode == .never
                    {}
                    else
                    {
                        return self.restore(edgeInsets: snapshot.scrollIndicatorInsets, attributedAlignment: attributedAlignment, command: command)
                    }
                    
                    return nil
                }
            }
        }
        
        return nil
    }
    
    private func update(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        if attributedAlignment.isDescendant
        {
            return updateDescendant(attributedAlignment: attributedAlignment, command: command)
        }
        
        return updateRoot(attributedAlignment: attributedAlignment, command: command)
    }
    
    private func updateDescendant(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        return nil
    }
    
    private func updateRoot(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        var scrollIndicatorInsets: UIEdgeInsets = command.scrollView.scrollIndicatorInsets
        
        let scrollLayerInWindow: CGRect = self.coordinateSpace.convert(view: command.scrollView, to: command.scrollView.window!)
        
        let intersectionRect: CGRect = scrollLayerInWindow.intersection(command.attributedAlignment.contentInset.frameEnd)
        
        if #available(iOS 11.0, *)
        {
            switch command.scrollView.contentInsetAdjustmentBehavior {
            case .automatic:
                scrollIndicatorInsets.bottom = command.attributedAlignment.contentInset.frameEnd.size.height - command.scrollView.safeAreaInsets.bottom
                
            case .always, .scrollableAxes:
                if intersectionRect.isNull == false
                {
                    scrollIndicatorInsets.bottom = intersectionRect.height
                }
                
            case .never:
                break
            }
        }
        else
        {
            if attributedAlignment.contentInset.mode == .always
            {
                if intersectionRect.isNull == false
                {
                    scrollIndicatorInsets = attributedAlignment.scrollIndicatorInsets.edgeInsets
                }
            }
        }
        
        let action = SVScrollIndicatorInsetsAnimation(scrollView: command.scrollView, toValue: scrollIndicatorInsets)
        
        action.animation = attributedAlignment.scrollIndicatorInsets.animation
        
        let plan = SVPlan(name: .scrollIndicatorInsets, command: command)
        
        plan.add(action: action)
        
        return plan
    }
    
    private func restore(edgeInsets: UIEdgeInsets, attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        let action = SVScrollIndicatorInsetsAnimation(scrollView: command.scrollView, toValue: edgeInsets)
        
        action.animation = attributedAlignment.scrollIndicatorInsets.animation
        
        let plan = SVPlan(name: .scrollIndicatorInsets, command: command)
        
        plan.add(action: action)
        
        return plan
    }
}
