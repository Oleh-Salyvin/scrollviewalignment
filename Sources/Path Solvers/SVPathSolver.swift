//
//  SVPathSolver.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 30.08.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import Foundation

final class SVPathSolver
{
    required init() {}

    func handle(command: SVCommand) -> SVPlan?
    {
        guard command.scrollView.window != nil else { return nil }
        
        if command.name == .willShow
        {
            switch command.attributedAlignment.path {
            case .automatic:
                let attributedAlignmentAutomatic = SVAttributedAlignment()
                
                attributedAlignmentAutomatic.path = .automatic
                
                return self.createPath(attributedAlignment: attributedAlignmentAutomatic, command: command)
                
            default:
                return self.createPath(attributedAlignment: command.attributedAlignment, command: command)
            }
        }
        
        return nil
    }
    
    private func createPath(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        switch attributedAlignment.path {
        case .automatic:
            let pathSolver = SVAutomaticPathSolver()
            
            return pathSolver.handle(attributedAlignment: attributedAlignment, command: command)
            
        case let .moveTo(_position):
            switch _position {
            case .anchor(_, _):
                let position = SVAnchorPosition()
                
                if let _point = position.handle(attributedAlignment: attributedAlignment, command: command)
                {
                    let action = SVPositionAnimation(scrollView: command.scrollView, toValue: _point)
                    
                    action.animation = attributedAlignment.animation
                    
                    let plan = SVPlan(name: .path, command: command)
                    
                    plan.add(action: action)
                    
                    return plan
                }
                
            case .point(_, _):
                let position = SVPointPosition()
                
                if let _point = position.handle(attributedAlignment: attributedAlignment, command: command)
                {
                    let action = SVPositionAnimation(scrollView: command.scrollView, toValue: _point)
                    
                    action.animation = attributedAlignment.animation
                    
                    let plan = SVPlan(name: .path, command: command)
                    
                    plan.add(action: action)
                    
                    return plan
                }
                
            case .toRect(_):
                let position = SVToInnerAlignmentRectPosition()
                
                if let _point = position.handle(attributedAlignment: attributedAlignment, command: command)
                {
                    let action = SVPositionAnimation(scrollView: command.scrollView, toValue: _point)
                    
                    action.animation = attributedAlignment.animation
                    
                    let plan = SVPlan(name: .path, command: command)
                    
                    plan.add(action: action)
                    
                    return plan
                }
            }
        }
        
        return nil
    }
}
