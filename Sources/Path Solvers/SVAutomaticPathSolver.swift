//
//  SVAutomaticPathSolver.swift
//  ScrollViewAlignment
//
//  Created by Oleh Salyvin on 25.09.2018.
//  Copyright © 2018 Oleh Salyvin. All rights reserved.
//

import UIKit

final class SVAutomaticPathSolver
{
    required init() {}

    private let coordinateSpace = SVCoordinateSpace()

    func handle(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVPlan?
    {
        guard command.scrollView.window != nil else { return nil }
        
        let innerRect: CGRect = self.coordinateSpace.visibleRect(attributedAlignment: attributedAlignment, scrollView: command.scrollView, keyboardFrame: command.attributedAlignment.contentInset.frameEnd)
        
        let objectRect = self.coordinateSpace.convert(view: command.view, to: command.scrollView)
        
        if command.name == .willShow, innerRect.contains(objectRect) == false
        {
            let plan = SVPlan(name: .path, command: command)
            
            if let _action = self.position_1_0(attributedAlignment: attributedAlignment, command: command)
            {
                plan.add(action: _action)
            }
            
            if let _action = self.position_1_1(attributedAlignment: attributedAlignment, command: command)
            {
                plan.add(action: _action)
            }
            
            return plan
        }
        else if command.name == .willShow, innerRect.contains(objectRect)
        {
            if let _action = self.position_2_0(attributedAlignment: attributedAlignment, command: command)
            {
                let plan = SVPlan(name: .path, command: command)
                
                plan.add(action: _action)
                
                return plan
            }
        }
        
        return nil
    }
    
    private func position_1_0(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVActionProtocol?
    {
        let anchor = SVAnchorPosition()
        
        let attributedAlignmentCustom = SVAttributedAlignment(descendant: attributedAlignment.isDescendant)
        
        attributedAlignmentCustom.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
        
        if let _position = anchor.handle(attributedAlignment: attributedAlignmentCustom, command: command)
        {
            let action = SVPositionAnimation(scrollView: command.scrollView, toValue: _position)
            
            var animation = attributedAlignment.animation
            animation.delay = 0.8
            animation.duration = 2
            
            action.animation = animation
            
            return action
        }
        
        return nil
    }
    
    private func position_1_1(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVActionProtocol?
    {
        let toRect = SVToInnerAlignmentRectPosition()
        
        let attributedAlignmentCustom = SVAttributedAlignment(descendant: attributedAlignment.isDescendant)
        
        attributedAlignmentCustom.path = .moveTo(.toRect(.visibleRect))
        
        if let _position = toRect.handle(attributedAlignment: attributedAlignmentCustom, command: command)
        {
            let action = SVPositionAnimation(scrollView: command.scrollView, toValue: _position)
            
            var animation = attributedAlignment.animation
            animation.delay = 0.1665
            animation.duration = 0.75
            animation.options = [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews, UIView.AnimationOptions.curveLinear]
            
            action.animation = animation
            
            return action
        }
        
        return nil
    }
    
    private func position_2_0(attributedAlignment: SVAttributedAlignment, command: SVCommand) -> SVActionProtocol?
    {
        let anchor = SVAnchorPosition()
        
        let attributedAlignmentCustom = SVAttributedAlignment(descendant: attributedAlignment.isDescendant)
        
        attributedAlignmentCustom.path = .moveTo(.anchor(SVAttributedAlignment.AnchorPointCenter, .visibleRect))
        
        if let _position = anchor.handle(attributedAlignment: attributedAlignmentCustom, command: command)
        {
            let action = SVPositionAnimation(scrollView: command.scrollView, toValue: _position)
            
            var animation = attributedAlignment.animation
            animation.delay = 0.1665
            animation.duration = 0.5
            animation.options = [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.layoutSubviews, UIView.AnimationOptions.curveLinear]
            
            action.animation = animation
            
            return action
        }
        
        return nil
    }
}
